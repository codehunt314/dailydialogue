from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, render_to_response
from django.template import RequestContext, loader
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth import get_user
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail, BadHeaderError

import string, random
import json, random, urllib2
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
from urlparse import urljoin
from news.models import *


import  mailchimp
mailchimp_client    =   mailchimp.Mailchimp(apikey='834c3aff9e9c3b67a8e90bca1f404e07-us10')


def id_generator(size=10, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def admin_dashboard(request):
    return render_to_response('admin_dashboard.html', {}, context_instance=RequestContext(request))


def adduser(request):
	if request.method=='POST':
		screen_name = request.POST.get('screen_name', None)
		if screen_name:
			user_profile, created   =   UserProfile.objects.get_or_create(screen_name=screen_name)
	return HttpResponseRedirect('/admin_dashboard/')


def fetch_user_news(request):
	if request.method=='POST':
		screen_name = request.POST.get('screen_name', None)
		if screen_name:
			user_profile, created   =   UserProfile.objects.get(screen_name=screen_name)
			user_profile.fetch_all()	#need to put it queue
	return HttpResponseRedirect('/admin_dashboard/')


def youtube(url):
	regex = re.compile(r"^(http://|https://)?(www\.)?(youtube\.com/watch\?v=)?(?P<id>[A-Za-z0-9\-=_]{11})")
	match = regex.match(url)
	if not match: return ""
	video_id = match.group('id')
	return """
	<object style="height: 390px; width: 640px">
	<param name="movie" value="http://www.youtube.com/v/%s?version=3"></param>
	<param name="allowFullScreen" value="true"></param>
	<param name="allowScriptAccess" value="always">
	<embed src="http://www.youtube.com/v/%s?version=3" type="application/x-shockwave-flash" allowfullscreen="true" allowScriptAccess="always" width="640" height="390">
	</object>
	""" % (video_id, video_id)


def new_home(request):
	news_results 	= 	[]
	for item in NewsResource.objects.all()[:50]:
		if item.newsurl_source and item.newsurl_source!='twitter.com':
			if item.newsurl_source=='www.youtube.com':
				item.__setattr__('summary', youtube(item.expanded_newsurl))
			else:
				item.__setattr__('summary', item.get_short_summary())
				item.__setattr__('images', item.get_images())
			news_results.append(item)
	# import pdb; pdb.set_trace()
	return render_to_response('new_home.html', {'news_results': news_results}, context_instance=RequestContext(request))

