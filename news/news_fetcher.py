import os, sys
from dateutil import parser
import twitter


PROJECT_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(PROJECT_PATH, '../'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dailydialogue.settings")
import django
django.setup()


from news.models import FuzzyCategory, NewsViaFriends, populate_fuzzyCategory
from userauth.twitterauth import TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET
from userauth.models import UserProfile

api 	=	twitter.Api(consumer_key=TWITTER_CONSUMER_KEY,
				consumer_secret=TWITTER_CONSUMER_SECRET,
				access_token_key='22993288-kNMIm2Qj4nrkOdQ4nrbEJ1Wznk8CijxpMtgfNK5M0',
				access_token_secret='MuTx77rc8mzgqvNwklY5J8vczcIgw6LivYMdYgiEKaqn0'
			)

new_urls = []


def get_user_timeline(screen_name):
	user_profile, created   =   UserProfile.objects.get_or_create(screen_name=screen_name)
	since_id 		= 	user_profile.since_id
	max_id 			= 	user_profile.max_id
	total_count 	= 	0
	fetch_url_count = 	0
	new_url_count 	=	0
	statuses 	= 	api.GetUserTimeline(screen_name=user_profile.screen_name, max_id=max_id, count=200, include_rts=True)
	total_count += len(statuses)
	idlist 		=	[]
	for item in statuses:
		idlist.append(item.id)
		fetch_url_count += len(item.urls)
		for each_url in item.urls:
			try:
				creation_time 		=	parser.parse(item.created_at)
				news_item, created 	= 	NewsViaFriends.objects.get_or_create(
												twitter_newsurl =   each_url.expanded_url,
												defaults 		=	{
													'userprofile' 	:	user_profile,
													'published_time': 	creation_time,
													'published_date':	creation_time.date(),
													'retweet_count' : 	item.GetRetweetCount(),
													'favorite_count':	item.GetFavoriteCount()
												}
											)
				if created:
					new_url_count += 1
				if not news_item.fetched:
					new_urls.append((news_item.id, news_item.twitter_newsurl))
			
				creation_time 		=	parser.parse(item.created_at)
				news_item, created 	= 	NewsViaFriends.objects.get_or_create(
												twitter_newsurl =   each_url.expanded_url,
												defaults 		=	{
													'userprofile' 	:	user_profile,
													'published_time': 	creation_time,
													'published_date':	creation_time.date(),
													'retweet_count' : 	item.GetRetweetCount(),
													#'favorite_count':	item.GetFavoriteCount()
												}
											)
				if created:
					new_url_count += 1
				if not news_item.fetched:
					new_urls.append((news_item.id, news_item.twitter_newsurl))
				# news_item.fetch_summary()
			except:
				pass
	if len(idlist)>0:
		max_id 		=	min(idlist) - 1
		since_id 	=	max(max(idlist), since_id)
	else:
		pass
	user_profile.since_id =	since_id
	user_profile.save()
	print 'Fetched/New urls = %s/%s' % (fetch_url_count, new_url_count)
	return True



import gevent.monkey
from gevent.pool import Pool
gevent.monkey.patch_socket()
gevent.monkey.patch_ssl()

import requests
requests.packages.urllib3.disable_warnings()

import logging
logging.getLogger("requests").setLevel(logging.WARNING)

from readability.readability import Document
from datetime import datetime
from bs4 import BeautifulSoup
from urlparse import urlparse
results = []


def fetch(param):
	# try:
	headers = 	{'User-agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5'}
	r 		= 	requests.get(param[1], headers=headers, verify=False)
	html 	=	r.content
	if urlparse(r.url).netloc == u'linkis.com':
		soup 	= 	BeautifulSoup(html)
		soup_a 	=	soup.find('a', attrs={'class': 'top-link js-original-link'})
		if soup_a:
			url 	=	soup.find('a', attrs={'class': 'top-link js-original-link'}).attrs['title']
			r 		= 	requests.get(url, headers=headers, verify=False)
			html 	=	r.content
	content_type	=	 r.headers.get('content-type', None)
	if content_type:
		if content_type.find('text/html')!=-1:
			title 	=	Document(html).short_title() if html!='' else ''
			summary = 	Document(html).summary() if html!='' else ''
		else:
			title 	=	r.url
			summary =	None
		results.append((param[0], title, r.url, summary))
		print title
	# except:
	# 	pass



if __name__ == "__main__":
	populate_fuzzyCategory()
	print '\n=======Fetching News Item=======\n'
	for fuzzyc in FuzzyCategory.objects.all():
		get_user_timeline(fuzzyc.userprofile.screen_name)
		# try:
		# 	get_user_timeline(fuzzyc.userprofile.screen_name)
		# except Exception, e:
		# 	print e
		# 	pass
	pool 	= 	Pool(len(new_urls))
	for newsitemid,url in new_urls:
		pool.spawn(fetch, (newsitemid, url))
	pool.join()
	new_newsitem_counter = 0
	for pk,title,url,summary in results:
		try:
			news_item = NewsViaFriends.objects.get(id=pk)
			news_item.newsurl_title 	= 	title
			news_item.expanded_newsurl 	= 	url
			news_item.newsurl_summary 	= 	summary
			news_item.fetched 			=	True
			news_item.save()
			new_newsitem_counter += 1
		except:
			# print 'ERROR:\t', pk,title,url
			pass
	print 'New News item: %s' % (new_newsitem_counter)


