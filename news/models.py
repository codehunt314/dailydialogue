from django.db import models
from userauth.models import UserProfile
import urllib2, urllib, cookielib
from readability.readability import Document
import requests
from urlparse import urlparse
from bs4 import BeautifulSoup


import re
from django.db import models
from userauth.models import UserProfile
import urllib2, urllib, cookielib
from readability.readability import Document
import requests
from urlparse import urlparse, urljoin
from bs4 import BeautifulSoup
import dateutil.parser as dateparser
from dateutil.parser import parse as date_parser
from news.getimageinfo import *


PUBLISH_DATE_TAGS = [
	{'attribute': 'property', 'value': 'rnews:datePublished', 'content': 'content'},
	{'attribute': 'property', 'value': 'article:published_time', 'content': 'content'},
	{'attribute': 'name', 'value': 'OriginalPublicationDate', 'content': 'content'},
	{'attribute': 'itemprop', 'value': 'datePublished', 'content': 'datetime'},
	{'attribute': 'property', 'value': 'og:published_time', 'content': 'content'},
	{'attribute': 'name', 'value': 'article_date_original', 'content': 'content'},
	{'attribute': 'name', 'value': 'publication_date', 'content': 'content'},
	{'attribute': 'name', 'value': 'sailthru.date', 'content': 'content'},
	{'attribute': 'name', 'value': 'PublishDate', 'content': 'content'},
	{'attribute': 'property', 'value': 'og:updated_time', 'content': 'content'},
	{'attribute': 'property', 'value': 'article:published_time', 'content': 'content'},
	{'attribute': 'property', 'value': 'article:modified_time', 'content': 'content'},
	{'attribute': 'itemprop', 'value': 'datePublished', 'content': 'content'},
	{'attribute': 'itemprop', 'value': 'dateModified', 'content': 'content'},
]


DATE_REGEX = r'([\./\-_]{0,1}(19|20)\d{2})[\./\-_]{0,1}(([0-3]{0,1}[0-9][\./\-_])|(\w{3,5}[\./\-_]))([0-3]{0,1}[0-9][\./\-]{0,1})?'

def _date_class_heuristic(css_class):
	if css_class:
		if css_class.find('date')!=-1: return True
		if css_class.find('time')!=-1: return True
		if css_class.find('timestamp')!=-1: return True
	return False

class NewsResource(models.Model):
	shared_by 			=	models.ForeignKey(UserProfile, blank=True, null=True)
	expanded_newsurl 	=	models.TextField()
	newsurl_title 		=	models.TextField(blank=True, null=True)
	newsurl_summary		=	models.TextField(blank=True, null=True)
	newsurl_article		=	models.TextField(blank=True, null=True)
	newsurl_source 		=	models.CharField(max_length=191, blank=True, null=True)
	published_time 		=	models.DateTimeField(auto_now=False, auto_now_add=True)
	discovered_time 	=	models.DateTimeField(auto_now=False, auto_now_add=True)
	author 				=	models.CharField(max_length=191, blank=True, null=True)
	author_twitterid 	=	models.CharField(max_length=191, blank=True, null=True)
	fetched 			=	models.BooleanField(default=False)
	retweet_count 		=	models.IntegerField(default=0)
	favorite_count 		=	models.IntegerField(default=0)
	status 				=	models.CharField(max_length=191, default='DISCOVERED', choices=(('DISCOVERED', 'DISCOVERED'), ('SELECTED', 'SELECTED'), ('STICKY', 'STICKY'), ('DELETED', 'DELETED')))

	def get_published_date(self):
		published_date = None
		
		#first approach
		
		date_match = re.search(DATE_REGEX, self.expanded_newsurl)
		if date_match:
			try:
				published_date = date_parser(date_match.group(0))
			except:
				published_date = None
		if published_date:
			self.published_time = published_date
			self.save()
		
		if self.newsurl_article:
			#second approach
			soup 	=	BeautifulSoup(self.newsurl_article)
			for known_meta_tag in PUBLISH_DATE_TAGS:
				meta_tag = soup.find(attrs={known_meta_tag['attribute']: known_meta_tag['value']})
				if meta_tag:
					date_str = meta_tag.attrs.get(known_meta_tag['content'])
					if date_str:
						print date_str
						try:
							published_date = date_parser(date_str)
						except:
							pass
			if published_date:
				self.published_time = published_date
				self.save()
			
			#third approach
			
			probable_dates = []
			date_classes_or_ids = []
			date_classes_or_ids.extend(soup.findAll(class_=_date_class_heuristic))
			date_classes_or_ids.extend(soup.findAll(id=_date_class_heuristic))
			for i in date_classes_or_ids:
				try:
					for txt in i.text.split('\n'):
						if txt.strip()!='':
							default_datetime = datetime.now()
							possible_date = dateparser.parse(txt.strip(), fuzzy=True, default=default_datetime)
							if possible_date!=default_datetime:
								probable_dates.append(possible_date)
				except Exception, e:
					pass
			probable_dates.sort()
			if len(probable_dates)>0:
				published_date = probable_dates[0]
			if published_date:
				self.published_time = published_date
				self.save()

	def get_short_summary(self):
		summary 	= 	[i.strip() for i in BeautifulSoup(self.newsurl_summary).text.split('\n') if len(i.strip())>100]
		text 		= 	'\n'.join(summary)
		word_list 	= 	text.split()
		word_limit 	=	min(100, int(len(word_list)*0.1))	
		return ' '.join(word_list[:word_limit])

	def get_images(self):
		resource_images = NewsResourceImages.objects.filter(news_resource=self).order_by('-width')
		if len(resource_images)>0:
			return resource_images[0].image_url
		# else:
		# 	resource_images = []
		# 	for i in BeautifulSoup(self.newsurl_summary).findAll('img'):
		# 		if not i.attrs.get('src', '').startswith('http://'):
		# 			img_link 	= 	urljoin(self.expanded_newsurl, i.attrs.get('src'))
		# 		else:
		# 			img_link 	= 	i.attrs.get('src')
		# 		width, height, content_type = getImageInfo(urllib.urlopen(img_link))
		# 		if content_type in ['image/gif', 'image/png', 'image/png', 'image/jpeg'] and width>300:
		# 			image, created = NewsResourceImages.objects.get_or_create(news_resource=self, image_url=img_link, width=width, height=height)
		# 			resource_images.append(image)
		# 	if len(resource_images)>0:
		# 		return resource_images[0].image_url
		return None


class NewsResourceImages(models.Model):
	news_resource 		=	models.ForeignKey(NewsResource)
	image_url 			=	models.TextField()
	image_path 			=	models.ImageField(upload_to='news_media/',blank=True, null=True)
	width 				=	models.IntegerField(default=0)
	height 				=	models.IntegerField(default=0)
	size_inbyte 		=	models.CharField(max_length=100, blank=True, null=True)

