from django.db import models

from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from twitterauth import TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, TWITTER_CALLBACK, req_token_url, authorize_url, access_token_url
import urllib, urlparse, time, oauth, json
from datetime import datetime
from dateutil import parser

import os, sys
import twitter
import grequests

from bs4 import BeautifulSoup
from readability.readability import Document
from urlparse import urlparse

api     =   twitter.Api(consumer_key=TWITTER_CONSUMER_KEY,
                consumer_secret=TWITTER_CONSUMER_SECRET,
                access_token_key='22993288-kNMIm2Qj4nrkOdQ4nrbEJ1Wznk8CijxpMtgfNK5M0',
                access_token_secret='MuTx77rc8mzgqvNwklY5J8vczcIgw6LivYMdYgiEKaqn0'
            )


class UserProfile(models.Model):
    user                =   models.OneToOneField(User, blank=True, null=True)
    twitterid           =   models.CharField(max_length=191, blank=True, null=True, editable=False)
    screen_name         =   models.CharField(max_length=191, blank=True, null=True, editable=False)
    access_token        =   models.CharField(max_length=191, blank=True, null=True, editable=False)
    access_token_secret =   models.CharField(max_length=191, blank=True, null=True, editable=False)
    profile_image_url   =   models.URLField(max_length=191, blank=True, null=True)
    location            =   models.CharField(max_length=100, blank=True, null=True)
    website             =   models.URLField(max_length=191, blank=True, null=True)
    description         =   models.CharField(max_length=160, blank=True, null=True)
    since_id            =   models.CharField(max_length=191, blank=True, null=True)
    max_id              =   models.CharField(max_length=191, blank=True, null=True)
    max_prcessed_id     =   models.CharField(max_length=191, default='0')
    last_tweet_time     =   models.DateTimeField(blank=True, null=True)
 
    def __str__(self):
        return "%s's profile" % self.user

    def get_user_timeline(self):
        from news.models import NewsResource
        since_id        =   self.since_id
        max_id          =   self.max_id
        statuses        =   api.GetUserTimeline(screen_name=self.screen_name, max_id=max_id, count=200, include_rts=True)
        tweet_time_list =   [self.last_tweet_time] if self.last_tweet_time else []
        for item in statuses:
            tweet_time  =   parser.parse(item.created_at)
            print tweet_time
            tweet_time_list.append(tweet_time)
            for each_url in item.urls:
                news_item, created  =   NewsResource.objects.get_or_create(
                                            expanded_newsurl    =   each_url.expanded_url,
                                            shared_by           =   self,
                                            published_time      =   tweet_time,
                                            discovered_time     =   tweet_time,
                                            retweet_count       =   item.retweet_count,
                                            favorite_count      =   item.favorite_count
                                        )
        self.last_tweet_time = max(tweet_time_list)
        self.save()

    def fetch_all(self):
        from news.models import NewsResource
        headers             =   {'User-agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5'}
        url_requests        =   [(nr.id, grequests.get(nr.expanded_newsurl, headers=headers)) for nr in NewsResource.objects.filter(shared_by=self) if not nr.fetched]
        url_requests_res    =   grequests.map([i[1] for i in url_requests])
        for index in range(len(url_requests_res)):
            try:
                response        =   url_requests_res[index]
                if response and response.status_code==200:
                    html            =   response.content
                    news_item       =   NewsResource.objects.get(id=url_requests[index][0])
                    news_item.expanded_newsurl   =   response.url
                    news_item.newsurl_title      =   Document(html).short_title() if html!='' else ''
                    news_item.newsurl_summary    =   Document(html).summary() if html!='' else ''
                    news_item.newsurl_article    =   str(BeautifulSoup(html))
                    news_item.newsurl_source     =   urlparse(response.url).netloc
                    news_item.fetched            =   True
                    news_item.save()
                else:
                    pass
            except Exception, e:
                pass


def init_userprofile():
    data = {
                'News & Opinion': [
                    'Vikram_Sood', 'authoramish', 'ShekharGupta', 'rajivchn', 'TheJaggi', 'toralvaria', 'arvindsubraman', 
                    'anilkohli54', 'barugaru', 'ARangarajan1972', 'APanagariya', 'ShivAroor', 'ravikailas', 'dhume', 
                    'sunandavashisht', 'Jairam_Ramesh', 'NITIAayog', 'pbmehta', 'mediacrooks', 'praveenswami', 
                    'kaushikcbasu', 'RMantri', 'MohanCRaja', 'sankrant', 'ajaishukla', 'bibekdebroy', 'acorn', 
                    'sgurumurthy', 'minhazmerchant', 'pravchak', 'SrinathRaghava2', 'Chopsyturvey', 'SwarajyaMag', 
                    'swapan55', 'Chellaney', 'Mint_Opinion', 'chitraSD', 'theUdayB', 'ShankkarAiyar', 'prasannavishy', 
                    'realitycheckind', 'vivakermani', 'Moneylifers']
    }
    for k, v in data.items():
        for user_name in v:
            user_profile, created   =   UserProfile.objects.get_or_create(screen_name=user_name)


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        profile, created = UserProfile.objects.get_or_create(user=instance)
 
post_save.connect(create_user_profile, sender=User)

