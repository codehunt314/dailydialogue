from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth import authenticate, login, logout, get_user

from twitterauth import *


def callback(request):
    if request.GET.get('oauth_token') and request.GET.get('oauth_verifier'):
        oauth_token         =   request.GET.get('oauth_token')
        request_token_dict  =   request.session['request_token']
        oauth_token_secret  =   request_token_dict.get('oauth_token_secret')
        token               =   oauth.OAuthToken(oauth_token, oauth_token_secret)
        access_token_parms  =   {
                'oauth_consumer_key'    :   TWITTER_CONSUMER_KEY,
                'oauth_nonce'           :   oauth.generate_nonce(),
                'oauth_signature_method':   "HMAC-SHA1",
                'oauth_timestamp'       :   str(int(time.time())),
                'oauth_token'           :   oauth_token,
                'oauth_verifier'        :   request.GET.get('oauth_verifier')
            }
        consumer                =   oauth.OAuthConsumer(key=TWITTER_CONSUMER_KEY, secret=TWITTER_CONSUMER_SECRET)
        req                     =   oauth.OAuthRequest(http_method="GET", http_url=access_token_url, parameters=access_token_parms)
        signature               =   oauth.OAuthSignatureMethod_HMAC_SHA1().build_signature(req,consumer,token)
        req.set_parameter('oauth_signature', signature)
        content                 =   urllib.urlopen(req.to_url()).read()
        access_token            =   dict(urlparse.parse_qsl(content))
        request.session['access_token']    =    access_token
        auth_user               =   authenticate(access_token=access_token)
        if auth_user:
            login(request, auth_user)
        else:
            #authentication failed, remove session information, this shouldn't happen.
            del request.session['access_token']
            del request.session['request_token']
            return HttpResponse('Unable to authenticate you!')
        return HttpResponseRedirect('/')
    else:
        return HttpResponse("no request token and verifier, should make request for request_token")


def joinus(request):
    access_token     =     request.session.get('access_token')
    if access_token:
        userprofile     =   UserProfile.objects.get(
                                access_token        =   access_token.get('oauth_token'), 
                                access_token_secret =   access_token.get('oauth_token_secret')
                            )
        userprofile.user.backend = 'userauth.twitterauth.TwitterBackend'
        login(request, userprofile.user)
        return HttpResponseRedirect('/')
    else:
        request_token                       =   twitter_get_request_token()
        print request_token
        request.session['request_token']    =   request_token
        article_to_vote                     =   request.GET.get('upvote')
        if article_to_vote:
            request.session['article_to_vote']  =  article_to_vote
        url_to_authorize                    =   "%s?oauth_token=%s" % (authorize_url, request_token['oauth_token'])
        return HttpResponseRedirect(url_to_authorize)


def signout(request):
    logout(request)
    return HttpResponseRedirect('/')


def admin_login(request):
    if request.method=='POST':
        username    =   request.POST['username']
        password    =   request.POST['password']
        user        =   authenticate(username=username, password=password)
        if user is not None:
            if user.is_active and user.is_staff:
                login(request, user)
                return HttpResponseRedirect('/admin_dashboard/')
            else:
                return HttpResponse('permission denied')
        else:
            return HttpResponse('invalid login')
    return render_to_response('staff_login.html', {}, context_instance=RequestContext(request))


def admin_logout(request):
    logout(request)
    return HttpResponseRedirect('/')

