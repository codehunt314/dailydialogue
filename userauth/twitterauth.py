from datetime import datetime
import urllib, urlparse, time, oauth, json
from userauth.models import *


TWITTER_CONSUMER_KEY         =  'cRMiuNGvgEksx6iwX9SBbv5uR'
TWITTER_CONSUMER_SECRET      =  'tJVqdHjb2fUbmYqul1vZFNvfYjHVpnhfiocIxEgkEfTD685n72'
TWITTER_CALLBACK        =   'http://127.0.0.1:8001/complete/twitter/'
req_token_url           =   'https://api.twitter.com/oauth/request_token'
authorize_url           =   "https://api.twitter.com/oauth/authorize"
access_token_url         =    "https://api.twitter.com/oauth/access_token"


class TwitterBackend:
    def authenticate(self, access_token):
        try:
            twitter_user_profile = get_user_info(access_token)
        except:
            return None
        user, created = User.objects.get_or_create(username=twitter_user_profile.get('screen_name'))
        if created:
            random_password = User.objects.make_random_password(length=6)
            user.set_password(random_password)
        user.first_name = twitter_user_profile.get('name')
        user.save()

        userprofile                 =   user.get_profile()
        userprofile.twitterid       =   access_token.get('user_id')
        userprofile.screen_name     =   access_token.get('screen_name')
        userprofile.access_token    =   access_token.get('oauth_token')
        userprofile.access_token_secret    =   access_token.get('oauth_token_secret')
        userprofile.website         =   twitter_user_profile.get('url')
        userprofile.location        =   twitter_user_profile.get('location')
        userprofile.description     =   twitter_user_profile.get('description')
        userprofile.profile_image_url = twitter_user_profile.get('profile_image_url')
        userprofile.save()
        return user

    def get_user(self, id):
        try:
            return User.objects.get(pk=id)
        except:
            return None


def twitter_get_request_token():
    params = {

        'oauth_timestamp'       :   str(int(time.time())),
        'oauth_signature_method':   "HMAC-SHA1",
        'oauth_version'         :   "1.0",
        'oauth_callback'        :   TWITTER_CALLBACK,
        'oauth_nonce'           :   oauth.generate_nonce(),
        'oauth_consumer_key'    :   TWITTER_CONSUMER_KEY
    }
    consumer        =   oauth.OAuthConsumer(key=TWITTER_CONSUMER_KEY, secret=TWITTER_CONSUMER_SECRET)
    req             =   oauth.OAuthRequest(http_method="GET", http_url=req_token_url, parameters=params)
    signature       =   oauth.OAuthSignatureMethod_HMAC_SHA1().build_signature(req,consumer,None)
    req.set_parameter('oauth_signature', signature)
    content         =   urllib.urlopen(req.to_url()).read()
    request_token   =   dict(urlparse.parse_qsl(content))
    return request_token


def get_user_info(access_token):
    user_lookup_url        = 'https://api.twitter.com/1.1/account/verify_credentials.json'
    params                 =    {
                                'oauth_consumer_key'    :   TWITTER_CONSUMER_KEY,
                                'oauth_nonce'           :   oauth.generate_nonce(),
                                'oauth_signature_method':   "HMAC-SHA1",
                                'oauth_timestamp'       :   str(int(time.time())),
                                'oauth_token'           :   access_token.get('oauth_token'),
                            }
    req             =   oauth.OAuthRequest(http_method="GET", http_url=user_lookup_url, parameters=params)
    consumer        =   oauth.OAuthConsumer(key=TWITTER_CONSUMER_KEY, secret=TWITTER_CONSUMER_SECRET)
    token           =   oauth.OAuthToken(access_token.get('oauth_token'), access_token.get('oauth_token_secret'))
    signature       =   oauth.OAuthSignatureMethod_HMAC_SHA1().build_signature(req,consumer,token)
    req.set_parameter('oauth_signature', signature)
    content                 =   urllib.urlopen(req.to_url()).read()
    return json.loads(content)


def get_user_timelineurl(screen_name=None, user_id=None, since_id=None):
    timeline_url = 'https://api.twitter.com/1.1/statuses/user_timeline.json'
    access_token = {'oauth_token': '22993288-kNMIm2Qj4nrkOdQ4nrbEJ1Wznk8CijxpMtgfNK5M0', 'oauth_token_secret': 'MuTx77rc8mzgqvNwklY5J8vczcIgw6LivYMdYgiEKaqn0'}
    params                 =    {
                                    'oauth_consumer_key'    :   TWITTER_CONSUMER_KEY,
                                    'oauth_nonce'           :   oauth.generate_nonce(),
                                    'oauth_signature_method':   "HMAC-SHA1",
                                    'oauth_timestamp'       :   str(int(time.time())),
                                    'oauth_token'           :   access_token.get('oauth_token'),
                                    'include_rts'           :   1
                                }
    if screen_name:
        params['screen_name'] = screen_name
    elif user_id:
        params['user_id'] = user_id
    else:
        return None
    if since_id:
        params['since_id'] = since_id
    req             =   oauth.OAuthRequest(http_method="GET", http_url=timeline_url, parameters=params)
    consumer        =   oauth.OAuthConsumer(key=TWITTER_CONSUMER_KEY, secret=TWITTER_CONSUMER_SECRET)
    token           =   oauth.OAuthToken(access_token.get('oauth_token'), access_token.get('oauth_token_secret'))
    signature       =   oauth.OAuthSignatureMethod_HMAC_SHA1().build_signature(req,consumer,token)
    req.set_parameter('oauth_signature', signature)
    content         =   json.loads(urllib.urlopen(req.to_url()).read())
    new_urls        =   []
    for c in content:
        for e in c.get('entities', {}).get('urls', []):
            new_urls.append(e.get('expanded_url'))
    new_since_id    =   max([c['id'] for c in content])
    return new_since_id, new_urls


def get_user_friends(screen_name=None, user_id=None):
    friends_url  = 'https://api.twitter.com/1.1/friends/ids.json'
    access_token = {'oauth_token': '22993288-kNMIm2Qj4nrkOdQ4nrbEJ1Wznk8CijxpMtgfNK5M0', 'oauth_token_secret': 'MuTx77rc8mzgqvNwklY5J8vczcIgw6LivYMdYgiEKaqn0'}
    params                 =    {
                                    'oauth_consumer_key'    :   TWITTER_CONSUMER_KEY,
                                    'oauth_nonce'           :   oauth.generate_nonce(),
                                    'oauth_signature_method':   "HMAC-SHA1",
                                    'oauth_timestamp'       :   str(int(time.time())),
                                    'oauth_token'           :   access_token.get('oauth_token'),
                                    'screen_name'           :   screen_name,
                                    'count'                 :   1000
                                }
    if screen_name:
        params['screen_name'] = screen_name
    elif user_id:
        params['user_id'] = user_id
    else:
        return None
    req             =   oauth.OAuthRequest(http_method="GET", http_url=friends_url, parameters=params)
    consumer        =   oauth.OAuthConsumer(key=TWITTER_CONSUMER_KEY, secret=TWITTER_CONSUMER_SECRET)
    token           =   oauth.OAuthToken(access_token.get('oauth_token'), access_token.get('oauth_token_secret'))
    signature       =   oauth.OAuthSignatureMethod_HMAC_SHA1().build_signature(req,consumer,token)
    req.set_parameter('oauth_signature', signature)
    content         =   json.loads(urllib.urlopen(req.to_url()).read())
    return content


def get_news_for_user(screen_name=None, user_id=None):
    news_url = []
    friends = get_user_friends(screen_name=screen_name, user_id=user_id)
    result = get_user_timelineurl(screen_name=screen_name, user_id=user_id)
    for url in result[1]:
        news_url.append(url)
    for frnd_id in friends['ids']:
        result = get_user_timelineurl(user_id=frnd_id)
        for url in result[1]:
            news_url.append(url)
    return news_url


# get_news_for_user(screen_name='TheJaggi')
